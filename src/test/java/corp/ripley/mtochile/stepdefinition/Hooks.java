package corp.ripley.mtochile.stepdefinition;

import corp.ripley.mtochile.util.driver.DriverFactory;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

public class Hooks {

    public static WebDriver driver;
    public Scenario scenario;
    public static String nombreTest;

    @Before
    //Delete all cookies at the start of each scenario to avoid shared state between tests
    public void openBrowser(Scenario scenario) {
        this.scenario = scenario;
        nombreTest = scenario.getName();
        if(DriverFactory.getDriver()!= null){
            DriverFactory.getDriver().manage().deleteAllCookies();
            DriverFactory.getDriver().manage().window().maximize();
        }
    }

    @After
    //Embed a screenshot in test report if test is marked as failed
    public void tearDown(Scenario scenario){
        if(scenario.isFailed()) {
            try {
                scenario.write("Current Page URL is " + DriverFactory.getDriver().getCurrentUrl());
                final byte[] screenshot = ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            } catch (WebDriverException somePlatformsDontSupportScreenshots) {
                System.err.println(somePlatformsDontSupportScreenshots.getMessage());
            }
        }
        DriverFactory.getDriver().quit();
        DriverFactory.removeDriver();
    }

    @AfterStep
    public void screenshot(Scenario scenario){
        scenario.write("Current Page URL is " + DriverFactory.getDriver().getCurrentUrl());
        byte[] screenshot = ((TakesScreenshot)DriverFactory.getDriver()).getScreenshotAs(OutputType.BYTES);
        scenario.embed(screenshot, "image/png");
    }

}
