package corp.ripley.mtochile.stepdefinition;

import corp.ripley.mtochile.pageobject.mto.LoginMTOPO;
import corp.ripley.mtochile.pageobject.mto.MTOHomePO;
import corp.ripley.mtochile.pageobject.mto.ModuloVisualizacionPO;
import corp.ripley.mtochile.pageobject.mto.SeleccionSucursalPO;
import corp.ripley.mtochile.pageobject.tienda.*;
import corp.ripley.mtochile.util.driver.SharedDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

import static corp.ripley.mtochile.util.insertToMTO.insertToMTOChile;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TarjetaCreditoR {

    public static String numerodeOC = "";

    private final TiendaPO tiendaPO;
    private final BolsaDeCompraPO bolsaDeCompraPO;
    private final LoginCheckoutUsuarioPO loginCheckoutUsuarioPO;
    private final DespachoPO despachoPO;
    private final MetodoPagoPO metodoPagoPO;
    private final ConfirmacionPagoPO confirmacionPagoPO;
    private final WebPayServerPO webPayServerPO;
    private final ControlTransBankPO controlTransBankPO;
    private final OrdenDeCompraPO ordenDeCompraPO;
    private final LoginMTOPO loginMTOPO;
    private final ModuloVisualizacionPO moduloVisualizacionPO;
    private final MTOHomePO mtoHomePO;
    private final SeleccionSucursalPO seleccionSucursalPO;

    public TarjetaCreditoR(SharedDriver driver,
                           TiendaPO tiendaPO,
                           BolsaDeCompraPO bolsaDeCompraPO,
                           LoginCheckoutUsuarioPO loginCheckoutUsuarioPO,
                           DespachoPO despachoPO,
                           MetodoPagoPO metodoPagoPO,
                           ConfirmacionPagoPO confirmacionPagoPO,
                           WebPayServerPO webPayServerPO,
                           ControlTransBankPO controlTransBankPO,
                           OrdenDeCompraPO ordenDeCompraPO,
                           LoginMTOPO loginMTOPO,
                           ModuloVisualizacionPO moduloVisualizacionPO,
                           MTOHomePO mtoHomePO,
                           SeleccionSucursalPO seleccionoSucursalPO) {
        this.tiendaPO = tiendaPO;
        this.bolsaDeCompraPO = bolsaDeCompraPO;
        this.loginCheckoutUsuarioPO = loginCheckoutUsuarioPO;
        this.despachoPO = despachoPO;
        this.metodoPagoPO = metodoPagoPO;
        this.confirmacionPagoPO = confirmacionPagoPO;
        this.webPayServerPO = webPayServerPO;
        this.controlTransBankPO = controlTransBankPO;
        this.ordenDeCompraPO = ordenDeCompraPO;
        this.loginMTOPO = loginMTOPO;
        this.moduloVisualizacionPO = moduloVisualizacionPO;
        this.mtoHomePO = mtoHomePO;
        this.seleccionSucursalPO = seleccionoSucursalPO;
    }

    @Given("que genero compra con datos: {string} {string} {string} {string} {string}")
    public void queGeneroCompraConDatos(String arg0, String arg1, String arg2, String arg3, String arg4) {
        tiendaPO.abroPaginaPreproductiva("https://Simple:acidO@preprod.ripley.cl/");
        tiendaPO.ingresoSKUEnElBuscador(arg0);
        tiendaPO.buscoSKU();
        tiendaPO.agregoSKUaLaBolsa();
        tiendaPO.voyALaBolsa();
        bolsaDeCompraPO.apretoBotonContinuarHaciaLoginUsuario();
        loginCheckoutUsuarioPO.ingresoComoUsuarioInvitado("Alejandro", "Contreras", "16798290-5", "acontrerass@ripley.com");
        despachoPO.creoDireccionDeEnvio();
        despachoPO.apretoBotonContinuarHaciaElPago();
        metodoPagoPO.seleccionoTarjetaCredito();
        metodoPagoPO.presionoBotonParaConfirmarMedioDePagoEscogido();
        confirmacionPagoPO.aceptolasCondicionesDeCompra();
        confirmacionPagoPO.apretoBotonContinuarLuegoDeAceptarCondiciones();
        webPayServerPO.ingresoDatosDeTarjetaDeCredito(arg1, arg2);
        webPayServerPO.seleccionoNumeroDeCuotas();
        webPayServerPO.realizoPagoConTarjetaDeCredito();
        controlTransBankPO.ingresoDatosDeUsuarioEnControlTransbank(arg3, arg4);
        controlTransBankPO.apretoBotonContinuar();
        controlTransBankPO.aceptoTransaccion();
        numerodeOC = ordenDeCompraPO.guardoElNumeroDeOrdenDeCompra();
    }

    @Given("que genero compra tarjeta Ripley con datos: {string} {string} {string} {string} {string}")
    public void queGeneroCompraTarjetaRipleyConDatos(String arg0, String arg1, String arg2, String arg3, String arg4) {
        tiendaPO.abroPaginaPreproductiva("https://Simple:acidO@preprod.ripley.cl/");
        tiendaPO.ingresoSKUEnElBuscador(arg0);
        tiendaPO.buscoSKU();
        tiendaPO.agregoSKUaLaBolsa();
        tiendaPO.voyALaBolsa();
        bolsaDeCompraPO.apretoBotonContinuarHaciaLoginUsuario();
        loginCheckoutUsuarioPO.ingresoComoUsuarioRegistrado(arg3, arg4);
        despachoPO.apretoBotonContinuarHaciaElPago();
        metodoPagoPO.agregoCVV(arg2);
        metodoPagoPO.presionoBotonParaConfirmarMedioDePagoEscogido();
        confirmacionPagoPO.aceptolasCondicionesDeCompra();
        confirmacionPagoPO.apretoBotonContinuarLuegoDeAceptarCondiciones();
        numerodeOC = ordenDeCompraPO.guardoElNumeroDeOrdenDeCompra();
    }

    @Then("inserto OC a MTO")
    public void insertoOCAMTO() {
        try {
            insertToMTOChile(numerodeOC);
        } catch (SAXException | TransformerException | ParserConfigurationException | IOException e) {
            e.printStackTrace();
        }
    }

    @When("abro MTO en Chile")
    public void abroMTOEnChile() {
        loginMTOPO.realizoLoginEnMTOCL("aligare", "password");
        seleccionSucursalPO.seleccionoSucursal("10000 - Huerfanos");
        mtoHomePO.ingresoAModuloDeOrdenes();
    }

    @Then("la visualizo en MTO Chile")
    public void laVisualizoEnMTOChile() {
        moduloVisualizacionPO.buscoOrdenDeCompraEnModuloDeVisualizacion(TarjetaCreditoR.numerodeOC);
    }

    @And("el estado es {string}")
    public void elEstadoEs(String arg0) {
        assertEquals(arg0, moduloVisualizacionPO.getEstadoOCEnMTO());
    }
}
