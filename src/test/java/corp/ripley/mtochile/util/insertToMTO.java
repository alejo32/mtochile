package corp.ripley.mtochile.util;

import okhttp3.*;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;

/**
 * Clase utilitaria que permite insertar una OC a MTO
 */
public class insertToMTO {

    /**
     * Método que permite insertar a MTO una orden de Compra
     * @param nroOrden Recibe número de Orden en formato String
     * @throws SAXException
     * @throws TransformerException
     * @throws ParserConfigurationException
     * @throws IOException
     */
    public static void insertToMTOPeru(String nroOrden) throws SAXException, TransformerException, ParserConfigurationException, IOException {
        // genero XML
        String xml = generateXML(
                "http://preprod-ibmcloud.ripley.com.pe/wcs/resources/store/10651/exportarOCDatos/get_exportarCajaVirtualRP/", nroOrden);

        // Creo un archivo XML
        stringToDom(xml);

        //Envio petición y obtengo json formateado
        String jsonText = obtainJsonToSend();

        // Envio a MTO
        sentToMTO(jsonText);
    }

    public static void insertToMTOChile(String nroOrden) throws SAXException, TransformerException, ParserConfigurationException, IOException {
        // genero XML
        String xml = generateXML(
                "https://preprod-ibmcloud.ripley.cl/wcs/resources/store/10051/exportarOCDatos/get_exportarCajaVirtualRP/", nroOrden);

        // Creo un archivo XML
        stringToDom(xml);

        //Envio petición y obtengo json formateado
        String jsonText = obtainJsonToSend();
        System.out.println(jsonText);

        // Envio a MTO
        sentToMTO(jsonText);
    }



    /**
     * Genera un String con contenido XML
     * @param url Url en formato String
     * @param nroOrdenCompra OC en formato String
     * @return Un String que contiene el texto del XML
     */
    public static String generateXML(String url, String nroOrdenCompra){
        String textXML = "";

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(url + nroOrdenCompra)
                .method("GET", null)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response.body() != null) {
                textXML = response.body().string();
            }
            //System.out.println(textXML);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return textXML;
    }

    /**
     * El contenido anterior de XML lo guarda en un archivo llamado my-file.xml
     * @param xmlSource Recibe un String con el contenido XML que usaremos para generar el archivo
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws TransformerException
     */
    public static void stringToDom(String xmlSource) throws SAXException, ParserConfigurationException, IOException, TransformerException {
        // Parse the given input
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(xmlSource)));

        // Write the parsed document to an xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);

        StreamResult result =  new StreamResult(new File("my-file.xml"));
        transformer.transform(source, result);
    }

    /**
     * Permite enviar el contenido del archivo my-file.xml a pal aplicación Heroku para su transformación a Json
     * @return un String con el contenido JSON
     * @throws IOException
     */
    public static String obtainJsonToSend() throws IOException {
        String jsonResponse = "";

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("text/plain");
        RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("archivo","my-file.xml",
                        RequestBody.create(MediaType.parse("application/octet-stream"),
                                new File("my-file.xml")))
                .build();
        Request request = new Request.Builder()
                .url("https://mto-trx-convert-xml-to-json.herokuapp.com/upload/mto/xmltojson/")
                .method("POST", body)
                .addHeader("api-key", "apitest")
                .build();
        Response response = client.newCall(request).execute();

        if (response.body() != null) {
            jsonResponse = response.body().string();
            //System.out.println(jsonResponse);
        }

        return jsonResponse;
    }

    /**
     * Permite inyectar a MTO la OC
     * @param jsonToSend Recibe como parametro el Json a enviar en el body de la petición
     * @throws IOException
     */
    public static void sentToMTO(String jsonToSend) throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType,  jsonToSend);
        Request request = new Request.Builder()
                .url("https://m4s2r7i7ph.execute-api.us-east-1.amazonaws.com/qa/send")
                .method("POST", body)
                .addHeader("x-api-key", "3SXxK8xEL31ralvQNE7XGapxkesW6NFO20drNKKH")
                //.addHeader("Authorization", "Basic NDIyNzA0NjQ6U3BrdHJvd282")
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = client.newCall(request).execute();

        System.out.println(response.body().string());
    }
}
