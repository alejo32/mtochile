package corp.ripley.mtochile.util.driver;

import corp.ripley.mtochile.util.util;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class SharedDriver {


	public SharedDriver() {
		String headless = System.getProperty("headless", "false");
		String webdriver = System.getProperty("browser", "chrome");
		String isOnDocker= System.getProperty("docker", "false");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-gpu");
		options.addArguments("--remote-debugging-port=9222");
		options.addArguments("--start-maximized");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("--disable-popup-blocking");
		//options.addArguments("--window-size=1366,768");
		options.addArguments("--incognito");

		if(headless.equals("true")) options.addArguments("headless");
		//options.setExperimentalOption("useAutomationExtension", false);
		//options.addArguments("--window-size=1366,768");
		//util.printCurrentThread();

		if (DriverFactory.getDriver() == null) {
			switch(webdriver) {
				case "firefox":
					WebDriverManager.firefoxdriver().setup();
					DriverFactory.addDriver(new FirefoxDriver());
					break;
				case "chrome":
					System.setProperty(ChromeDriverService.CHROME_DRIVER_LOG_PROPERTY, System.getProperty("user.dir") + "/target/chromedriver.log");
					if(isOnDocker.equals("false")) WebDriverManager.chromedriver().setup();
					DriverFactory.addDriver(new ChromeDriver(options));
					break;
				case "zalenium":
					try {
						options.addArguments("--window-size=1920,1080");
						DriverFactory.addDriver(new RemoteWebDriver(new URL("http://10.0.149.90:4442/wd/hub"), options));
					} catch (MalformedURLException e) {
						e.printStackTrace();
					}
					break;
				case "seleniumHub":
					try {
						options.addArguments("--window-size=1920,1080");
						options.addArguments("headless");
						DriverFactory.addDriver(new RemoteWebDriver(new URL("http://10.0.149.90:4444/wd/hub"), options));
					} catch (MalformedURLException e) {
						e.printStackTrace();
					}
					break;
				case "safari":
					//TODO: implement SafariDriver
					throw new RuntimeException("Unsupported webdriver: " + webdriver);
				default:
					throw new RuntimeException("Unsupported webdriver: " + webdriver);
			}
		}
	}
}
