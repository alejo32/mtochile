package corp.ripley.mtochile.pageobject.mto;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que utiliza patrón de diseño Page Object, en la cual se setean los localizadores de una página web
 * y los métodos de página
 * @url https://mtoweb-s.ripleyqa.com/index.html#/back-office/search
 * @author Alejandro Contreras
 * @version 1.0
 */
public class ModuloVisualizacionPO extends BasePage {

    // Constructor
    public ModuloVisualizacionPO() {
        super(DriverFactory.getDriver());
    }

    // Localizadores de Página
    @FindBy(how = How.NAME, using = "Orden")
    private WebElement campoIngresarOCaBuscar;

    @FindBy(how = How.XPATH, using = "//*[@class=\"button is-success\"][1]")
    private WebElement btnBuscarOC;

    @FindBy(how = How.XPATH, using = "//*[@class=\"table\"]/tbody/tr/th[6]")
    private WebElement estadoOC;

    @FindBy(how = How.XPATH, using = "//*[@class=\"button is-success\" and @type=\"reset\" and contains(text(), \"Limpiar\")]")
    private WebElement btnSiguiente;

    // Métodos de página
    public void buscoOrdenDeCompraEnModuloDeVisualizacion(String numeroOC){
        esperar(30); // --> Espera
        waitForElementToBeVisibleAndClickable(campoIngresarOCaBuscar);
        campoIngresarOCaBuscar.sendKeys(numeroOC);
        actionClick(btnBuscarOC);
        esperar(10);
    }

    public String getEstadoOCEnMTO(){
        String estadoOC = "";

        waitForElementToBeVisible(this.estadoOC);

        try {
            estadoOC = this.estadoOC.getText().trim();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

        return estadoOC;
    }
}
