package corp.ripley.mtochile.pageobject.mto;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que utiliza patrón de diseño Page Object, en la cual se setean los localizadores de una página web
 * y los métodos de página
 * @url https://mtoweb-s.ripleyqa.com/index.html#/login
 * @author Alejandro Contreras
 * @version 1.0
 */
public class LoginMTOPO extends BasePage {

    // Localizadores de página
    @FindBy(how = How.ID, using = "usuario")
    private WebElement campoUsuario;

    @FindBy(how = How.ID, using = "password")
    private WebElement campoPassword;

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname=\"cadena\"]/option[2]")
    private WebElement selectRipleyCL;

    @FindBy(how = How.XPATH, using = "//*[@formcontrolname=\"cadena\"]/option[3]")
    private WebElement selectRipleyPE;

    @FindBy(how = How.ID, using = "boton")
    private WebElement btnEntrar;

    // Constructor
    public LoginMTOPO() {
        super(DriverFactory.getDriver());
    }

    //Métodos de página
    public void realizoLoginEnMTOCL(String usuario, String password){
        // Primero cargo página web
        driver.get("https://mtoweb-s.ripleyqa.com/index.html#/login");

        // Luego espero carga de los elementos del Login
        waitForElementToBeVisible(this.campoUsuario);
        waitForElementToBeVisible(this.campoPassword);
        waitForElementToBeVisible(this.selectRipleyCL);
        waitForElementToBeVisible(this.selectRipleyPE);
        waitForElementToBeVisible(this.btnEntrar);

        // Luego realizo login
        this.campoUsuario.sendKeys(usuario);
        this.campoPassword.sendKeys(password);
        this.selectRipleyCL.click();
        this.btnEntrar.click();
    }

    public void realizoLoginEnMTOPE(String usuario, String password){
        // Primero cargo página web
        driver.get("https://mtoweb-s.ripleyqa.com/index.html#/login");

        // Luego espero carga de los elementos del Login
        waitForElementToBeVisible(this.campoUsuario);
        waitForElementToBeVisible(this.campoPassword);
        waitForElementToBeVisible(this.selectRipleyCL);
        waitForElementToBeVisible(this.selectRipleyPE);
        waitForElementToBeVisible(this.btnEntrar);

        // Luego realizo login
        this.campoUsuario.sendKeys(usuario);
        this.campoPassword.sendKeys(password);
        this.selectRipleyPE.click();
        this.btnEntrar.click();
    }
}
