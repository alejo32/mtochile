package corp.ripley.mtochile.pageobject.mto;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que utiliza patrón de diseño Page Object, en la cual se setean los localizadores de una página web
 * y los métodos de página
 * @url https://mtoweb-s.ripleyqa.com/index.html#/branch-office
 * @author Alejandro Contreras
 * @version 1.0
 */
public class SeleccionSucursalPO extends BasePage {

    // Constructor
    public SeleccionSucursalPO() {
        super(DriverFactory.getDriver());
    }

    // Selectores
    @FindBy(how = How.XPATH, using = "//*[@id=\"branchOfficeSelected\" and @placeholder=\"Ingrese la sucursal\"]")
    private WebElement selectSucursal;

    @FindBy(how = How.XPATH, using = "//*[@class=\"cajaConDatos\"]")
    private WebElement cajaDeDatos;


    // Métodos de página
    public void seleccionoSucursal(String sucursal){
        // Espero que elemento aparezca
        esperar(5);
        waitForElementToBeVisible(selectSucursal);

        // Hago click en campo Select
        selectSucursal.click();
        esperar(2);
        selectSucursal.sendKeys(sucursal , Keys.ENTER);
    }
}
