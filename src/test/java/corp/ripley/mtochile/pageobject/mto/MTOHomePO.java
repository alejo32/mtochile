package corp.ripley.mtochile.pageobject.mto;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que utiliza patrón de diseño Page Object, en la cual se setean los localizadores de una página web
 * y los métodos de página
 * @url https://mtoweb-s.ripleyqa.com/index.html#/back-office/welcome
 * @author Alejandro Contreras
 * @version 1.0
 */
public class MTOHomePO extends BasePage {

    // Constructor
    public MTOHomePO() {
        super(DriverFactory.getDriver());
    }

    // Localizadores de Página
    @FindBy(how = How.XPATH, using = "//*[@class=\"icono icono-order\"]")
    private WebElement logoOrdenes;

    @FindBy(how = How.XPATH, using = "//*[@class=\"hideMenu\" and @title=\">Visualización\"]")
    private WebElement moduloDeVisualizacionDeOrdenes;


    // Métodos de Página
    public void ingresoAModuloDeOrdenes(){
        esperar(3);
        waitForElementToBeVisibleAndClickable(logoOrdenes);
        actionClick(logoOrdenes);

        waitForElementToBeVisibleAndClickable(moduloDeVisualizacionDeOrdenes);
        actionClick(moduloDeVisualizacionDeOrdenes);
    }
}
