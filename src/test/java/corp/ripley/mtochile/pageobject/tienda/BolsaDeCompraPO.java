package corp.ripley.mtochile.pageobject.tienda;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que permite mapear los elementos de la Tienda Virtual Preroductiva de Chile
 * @url https://preprod.ripley.cl/bolsa
 * @author Alejandro Contreras
 * @version 1.0
 */
public class BolsaDeCompraPO extends BasePage {

    // Contructor
    public BolsaDeCompraPO() {
        super(DriverFactory.getDriver());
    }

    // Localizadores de página
    @FindBy(how = How.ID, using = "btn-continue")
    private WebElement btnContinuarCompra;

    @FindBy(how = How.XPATH, using = "//*[@class=\"shopping-bag-item\"]")
    private WebElement contenedorDeProductos;

    // Métodos de Página
    /**
     * Método que me permite continuar hacia el login-checkout del usuario
     * esto luego de agrgegar productos a la bolsa y tener intención de comprarlos
     */
    public void apretoBotonContinuarHaciaLoginUsuario(){
        waitForElementToBeVisible(this.contenedorDeProductos);
        waitForElementToBeVisibleAndClickable(this.btnContinuarCompra);
        this.btnContinuarCompra.click();
        esperar(2);
    }
}
