package corp.ripley.mtochile.pageobject.tienda;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que permite mapear los elementos de una Orden de Compra en la Tienda Virtual de Chile
 * @url https://checkout-preprod.ripley.cl/order-summary
 * @author Alejandro Contreras
 * @version 1.0
 */
public class OrdenDeCompraPO extends BasePage {

    public OrdenDeCompraPO() {
        super(DriverFactory.getDriver());
    }

    // Locators
    @FindBy(how = How.XPATH, using = "//*[@class=\"OrderSummaryDisplay_summaryContainer__11HoR\"]")
    private WebElement contenedorResumenOC;

    @FindBy(how = How.ID, using = "order-number")
    private WebElement campoNumeroDeOC;

    // Métodos de Página
    public String guardoElNumeroDeOrdenDeCompra(){
        String numeroDeOC = "";

        waitForElementToBeVisible(contenedorResumenOC);
        waitForElementToBeVisible(campoNumeroDeOC);

        try {
            numeroDeOC = campoNumeroDeOC.getText().trim();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

        System.out.println("El número de Orden De Compra es: " + numeroDeOC);

        return numeroDeOC;
    }
}
