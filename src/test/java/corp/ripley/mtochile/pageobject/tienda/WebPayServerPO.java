package corp.ripley.mtochile.pageobject.tienda;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que permite mapear los elementos de WebPay Server en la Tienda Virtual de Chile
 * @url https://webpay3gint.transbank.cl/webpayserver/dist/
 * @author Alejandro Contreras
 * @version 1.0
 */
public class WebPayServerPO extends BasePage {

    public WebPayServerPO() {
        super(DriverFactory.getDriver());
    }

    // Locators
    @FindBy(how = How.XPATH, using = "//*[@class=\"col-md-4 payment-method medios\"]")
    private WebElement contenedorDatosTarjeta;

    @FindBy(how = How.ID, using = "visa-card-show")
    private WebElement campoNumeroDeTarjeta;

    @FindBy(how = How.ID, using = "fechaExpiracion")
    private WebElement campoFechaVencimiento;

    @FindBy(how = How.ID, using = "password-invalid")
    private WebElement campoCVV;

    @FindBy(how = How.XPATH, using = "//*[@class=\"button new-marg next-padd\"]")
    private WebElement btnContinuar;

    @FindBy(how = How.XPATH, using = "//*[@class=\"col-md-12 credit-form pay-options\"]")
    private WebElement contenedorCantidadDeCuotas;

    @FindBy(how = How.XPATH, using = "//*[@class=\"dues-option\"]/option[1]")
    private WebElement selectSinCuotas;

    @FindBy(how = How.XPATH, using = "//*[@class=\"button new-marg next-padd\"]")
    private WebElement btnPagar;

    // Métodos de página
    public void ingresoDatosDeTarjetaDeCredito(String pan, String cvv){
        // Espero que contenedor sea visible
        waitForElementToBeVisible(this.contenedorDatosTarjeta);
        waitForElementToBeVisible(this.campoNumeroDeTarjeta);

        // Envío datos de tarjeta
        this.campoNumeroDeTarjeta.sendKeys(pan);
        this.campoFechaVencimiento.click();
        this.campoFechaVencimiento.sendKeys("1026");
        this.campoCVV.sendKeys(cvv);

        // Espero que botón continuar sea accesible y le doy a enter
        waitForElementToBeVisibleAndClickable(this.btnContinuar);
        this.btnContinuar.click();
    }

    public void seleccionoNumeroDeCuotas(){
        // Espero que contenedor sea visible
        waitForElementToBeVisible(this.contenedorCantidadDeCuotas);
        waitForElementToBeVisible(this.selectSinCuotas);

        // Selecciono sin cuotas
        this.selectSinCuotas.click();
    }

    public void realizoPagoConTarjetaDeCredito(){
        waitForElementToBeVisibleAndClickable(this.btnPagar);
        actionClick(this.btnPagar);
    }
}
