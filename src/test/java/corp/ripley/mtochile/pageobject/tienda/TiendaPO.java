package corp.ripley.mtochile.pageobject.tienda;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que permite mapear los elementos de la página Home Preroductiva de Chile
 * @url https://preprod.ripley.cl/
 */
public class TiendaPO extends BasePage {

    // Constructor
    public TiendaPO() {
        super(DriverFactory.getDriver());
    }

    // WebElements TIENDA RIPLEY PERÚ
    @FindBy(how = How.NAME, using = "searchTerm")
    private WebElement campoBusquedaSKU;

    @FindBy(how = How.XPATH, using = "//*[@class=\"svg-icon svg-icon-buscador\"]")
    private WebElement btnBuscarSKU;

    @FindBy(how = How.ID, using = "buy-button")
    private WebElement btnAgregarSKUaLaBolsa;

    @FindBy(how = How.XPATH, using = "//*[@class=\"modal-product-added-message\"]")
    private WebElement mensajeProductoAgregado;

    @FindBy(how = How.XPATH, using = "//*[@class=\"btn btn-ripley\"]")
    private WebElement btnIraLaBolsa;

    @FindBy(how = How.XPATH, using = "//*[@class=\"message-area message-error message-show\"]")
    private WebElement mensajeProductoAgotado;

    @FindBy(how = How.XPATH, using = "//*[@class=\"delivery-pickup-commune\"]")
    private WebElement contenedorPickComunas;

    // Métodos de página
    public void abroPaginaPreproductiva(String url){
        driver.get(url);
    }

    public void ingresoSKUEnElBuscador(String sku) {
        waitForElementToBeVisibleAndClickable(this.campoBusquedaSKU);
        this.campoBusquedaSKU.sendKeys(sku);
    }

    public void buscoSKU(){
        this.btnBuscarSKU.click();
    }

    public void agregoSKUaLaBolsa(){
        waitForElementToBeVisible(this.contenedorPickComunas);
        //esperar(2);
        waitForElementToBeVisibleAndClickable(this.btnAgregarSKUaLaBolsa);
        //esperar(2);
        this.btnAgregarSKUaLaBolsa.click();
        //esperar(2);
        waitForElementToBeVisible(this.mensajeProductoAgregado);
    }

    public void voyALaBolsa(){
        this.btnIraLaBolsa.click();
    }
}
