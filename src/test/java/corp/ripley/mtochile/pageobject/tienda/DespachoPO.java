package corp.ripley.mtochile.pageobject.tienda;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que permite mapear los elementos de selección de Despacho en la Tienda Virtual de Chile
 * @url https://checkout-preprod.ripley.cl/select-shipping
 * @author Alejandro Contreras
 * @version 1.0
 */
public class DespachoPO extends BasePage {

    public DespachoPO() {
        super(DriverFactory.getDriver());
    }

    // WebElements
    @FindBy(how = How.XPATH, using = "//*[@class=\"MainContent_main-not-padding__3I7d9 MainContent_main__DaMMR\"]")
    private WebElement contenedorSeleccionMetodoDespacho;

    @FindBy(how = How.XPATH, using = "//*[@class=\"TextInput_textInputControl__3MvNm\" and @name=\"auto\"]")
    private WebElement campoDireccion;

    @FindBy(how = How.XPATH, using = "//*[@class=\"AddressNormalizer_places__woZ8x\"]")
    private WebElement selectDireccion;

    @FindBy(how = How.XPATH, using = "//*[@class=\"TextInput_textInputControl__3MvNm\" and @name=\"apartmentNumber\"]")
    private WebElement campoNumeroDepto;

    @FindBy(how = How.NAME, using = "phone")
    private WebElement campoTelefono;

    @FindBy(how = How.XPATH, using = "//*[@class=\"Button_button__Wyg3z Button_primary__38apQ AddressForm_button__2c4Y6\"]")
    private WebElement btnCrearDireccion;

    @FindBy(how = How.XPATH, using = "//*[@class=\"ShippingGroup_group__2x7Oi\"]")
    private WebElement contenedorOpcionesDeEntrega;

    @FindBy(how = How.XPATH, using = "//*[@class=\"ContinueButton_continueButton__ODZjF ContinueButton_desktop__3FVAI undefined\"]")
    private WebElement btnContinuarHaciaPago;

    // Métodos de Página
    public void creoDireccionDeEnvio(){
        // Espera carga de elementos
        waitForElementToBeVisible(this.contenedorSeleccionMetodoDespacho);
        waitForElementToBeVisible(this.campoDireccion);

        // Envio de Datos a campo Nombre
        this.campoDireccion.sendKeys("Chile España 10, Ñuñoa");
        waitForElementToBeVisible(this.selectDireccion);
        actionClick(this.selectDireccion);

        // Envio de datos a campo Depto
        this.campoNumeroDepto.sendKeys("23");

        // Envio de datos a campo Telefono
        this.campoTelefono.sendKeys("955224477");
        esperar(2);

        // Aprieta botón crear Direccion
        waitForElementToBeVisibleAndClickable(this.btnCrearDireccion);
        actionClick(this.btnCrearDireccion);
    }

    public void apretoBotonContinuarHaciaElPago(){
        esperar(2);
        waitForElementToBeVisible(this.contenedorOpcionesDeEntrega);
        waitForElementToBeVisibleAndClickable(this.btnContinuarHaciaPago);
        actionClick(this.btnContinuarHaciaPago);
    }
}
