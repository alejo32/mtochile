package corp.ripley.mtochile.pageobject.tienda;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que permite mapear los elementos de Control Transbank en la Tienda Virtual de Chile
 * @url https://webpay3gint.transbank.cl/webpayserver/bp_control.cgi
 * @author Alejandro Contreras
 * @version 1.0
 */
public class ControlTransBankPO extends BasePage {

    public ControlTransBankPO() {
        super(DriverFactory.getDriver());
    }

    // Locators
    @FindBy(how = How.XPATH, using = "//*[@id=\"control\"]/frame[2]")
    private WebElement frame;

    @FindBy(how = How.XPATH, using = "//*[@id=\"rutClient\"]")
    private WebElement campoRutCliente;

    @FindBy(how = How.XPATH, using = "//*[@id=\"passwordClient\"]")
    private WebElement campoPassword;

    @FindBy(how = How.XPATH, using = "//*[@value=\"Aceptar\"]")
    private WebElement btnAceptar;

    @FindBy(how = How.XPATH, using = "//*[@id=\"vci\"]/option[1]")
    private WebElement selectAceptar;

    @FindBy(how = How.XPATH, using = "//*[@value=\"Continuar\"]")
    private WebElement btnContinuar;

    // mMétodos de página
    public void ingresoDatosDeUsuarioEnControlTransbank(String rut, String clave){
        // Cambio hacia el frame que contiene los elementos
        driver.switchTo().frame(this.frame);

        // Espero carga de elementos
        waitForElementToBeVisible(this.campoRutCliente);
        waitForElementToBeVisible(this.campoPassword);

        //Envío datos
        this.campoRutCliente.sendKeys(rut);
        this.campoPassword.sendKeys(clave);
    }

    public void apretoBotonContinuar(){
        waitForElementToBeVisibleAndClickable(this.btnAceptar);
        this.btnAceptar.click();
    }

    public void aceptoTransaccion(){
        // Cambio nuevamente al frame, ya que es nueva página
        this.selectAceptar.click();

        // Espero carga de elemento y hago click
        waitForElementToBeVisibleAndClickable(this.btnContinuar);
        this.btnContinuar.click();
    }
}
