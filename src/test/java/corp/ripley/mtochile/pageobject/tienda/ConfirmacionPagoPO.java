package corp.ripley.mtochile.pageobject.tienda;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que permite mapear los elementos de Confirmación de Pago en la Tienda Virtual de Chile
 * @url https://checkout-preprod.ripley.cl/confirmation
 * @author Alejandro Contreras
 * @version 1.0
 */
public class ConfirmacionPagoPO extends BasePage {

    // Constructor
    public ConfirmacionPagoPO() {
        super(DriverFactory.getDriver());
    }

    // Localizadores de página
    @FindBy(how = How.XPATH, using = "//*[@class=\"MainContent_main__DaMMR\"]")
    private WebElement contenedorResumenCompra;

    @FindBy(how = How.XPATH, using = "//*[@class=\"Sidebar_sidebar__3qUkX\"]")
    private WebElement contenedorCalculoDePrecio;

    @FindBy(how = How.XPATH, using = "//*[@class=\"Checkbox_checkbox__2LdRR\"]")
    private WebElement checkBoxAceptoCondicionesDeCompra;

    @FindBy(how = How.XPATH, using = "//*[@class=\"ContinueButton_continueButton__ODZjF ContinueButton_desktop__3FVAI undefined\"]")
    private WebElement btnContinuarHaciaPagoWebPay;

    // Métodos de Página
    public void aceptolasCondicionesDeCompra(){
        // Espero a que cargen elementos, antes de interactuar con la página
        waitForElementToBeVisible(this.contenedorResumenCompra);
        waitForElementToBeVisible(this.contenedorCalculoDePrecio);
        this.checkBoxAceptoCondicionesDeCompra.click();
        /*esperar(7);
        actionClick(this.btnContinuarHaciaPagoWebPay);*/
    }

    public void apretoBotonContinuarLuegoDeAceptarCondiciones(){
        esperar(4);
        actionClick(this.btnContinuarHaciaPagoWebPay);
    }
}
