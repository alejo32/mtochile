package corp.ripley.mtochile.pageobject.tienda;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que permite mapear los elementos de Selección de Métodos de Pago en la Tienda Virtual de Chile
 * @url https://checkout-preprod.ripley.cl/payment-methods
 * @author Alejandro Contreras
 * @version 1.0
 */
public class MetodoPagoPO extends BasePage {

    public MetodoPagoPO() {
        super(DriverFactory.getDriver());
    }

    // WebElements
    @FindBy(how = How.XPATH, using = "//*[@class=\"MainContent_main-white__1gnX_ MainContent_main__DaMMR\"]")
    private WebElement contenedorMediosDePago;

    @FindBy(how = How.XPATH, using = "//*[@class=\"WebpayCreditPaymentMethod_paymentMethod__2OQOW\"]")
    private WebElement radioButtonTarjetaDeCredito;

    @FindBy(how = How.XPATH, using = "//*[@class=\"ContinueButton_continueButton__ODZjF ContinueButton_desktop__3FVAI undefined\"]")
    private WebElement btnContinuarHaciaElPago;

    @FindBy(how = How.XPATH, using = "//*[@class=\"TransactionCompleteCardSelector_cardSelected__6tbA_\"][1]")
    private WebElement contenedorDeDatosTarjetaRipley;

    @FindBy(how = How.ID, using = "cvv")
    private WebElement campoCVV;


    //Métodos de Página
    public void seleccionoTarjetaCredito(){
        // Espero a que cargen elementos y luego presiono radio button Tarjeta de Crédito
        waitForElementToBeVisible(this.contenedorMediosDePago);
        waitForElementToBeVisible(this.radioButtonTarjetaDeCredito);
        esperar(2);
        this.radioButtonTarjetaDeCredito.click();
        actionClick(this.radioButtonTarjetaDeCredito);
    }

    public void presionoBotonParaConfirmarMedioDePagoEscogido(){
        waitForElementToBeVisibleAndClickable(this.btnContinuarHaciaElPago);
        this.btnContinuarHaciaElPago.click();
    }

    public void agregoCVV(String CVV) {
        waitForElementToBeVisible(this.contenedorMediosDePago);
        waitForElementToBeVisible(this.contenedorDeDatosTarjetaRipley);
        waitForElementToBeVisibleAndClickable(this.campoCVV);
        esperar(4);

        //this.campoCVV.click();
        this.campoCVV.sendKeys(CVV);
    }
}
