package corp.ripley.mtochile.pageobject.tienda;

import corp.ripley.mtochile.util.BasePage;
import corp.ripley.mtochile.util.driver.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Clase que permite mapear los elementos del Login o Checkout del usuario en la Tienda Virtual de Chile
 * @url https://preprod.ripley.cl/usuario/login-checkout
 * @author Alejandro Contreras
 * @version 1.0
 */
public class LoginCheckoutUsuarioPO extends BasePage {

    public LoginCheckoutUsuarioPO() {
        super(DriverFactory.getDriver());
    }

    @FindBy(how = How.XPATH, using = "//*[@class=\"login-checkout-form-wrapper\"][2]")
    private WebElement contenedorIngresarComoInvitado;

    @FindBy(how = How.CLASS_NAME, using = "login-checkout")
    private WebElement contenedorLoginCheckout;

    @FindBy(how = How.NAME, using = "firstname")
    private WebElement campoNombre;

    @FindBy(how = How.NAME, using = "lastname")
    private WebElement campoApellido;

    @FindBy(how = How.NAME, using = "nationalID")
    private WebElement campoRut;

    @FindBy(how = How.NAME, using = "email")
    private WebElement campoEmail;

    @FindBy(how = How.XPATH, using = "//*[@class=\"btn-loading  btn-ripley\" and contains(text(), \"Ingresar\")]")
    private WebElement btnIngresarComoInvitado;

    @FindBy(how = How.NAME, using = "ws_username")
    private WebElement campoRutUsuarioRegistrado;

    @FindBy(how = How.NAME, using = "password")
    private WebElement campoClaveUsuarioRegistrado;

    @FindBy(how = How.XPATH, using = "//*[@class=\"btn-loading  btn-ripley\"]")
    private WebElement btnIngresarComoUsuarioRegistrado;

    // Métodos de página
    /**
     * Método que permite ingresar a comprar a Ripley como usuario invitado
     * @param nombre nombre del usuario en formato String
     * @param apellido apellido del usuario en formato String
     * @param rut rut del usuario en formato String
     * @param email email del usuario en formato String
     */
    public void ingresoComoUsuarioInvitado(String nombre, String apellido, String rut, String email){
        waitForElementToBeVisible(this.contenedorIngresarComoInvitado);
        esperar(2);
        this.campoNombre.sendKeys(nombre);
        this.campoApellido.sendKeys(apellido);
        this.campoRut.sendKeys(rut);
        this.campoEmail.sendKeys(email);
        this.btnIngresarComoInvitado.click();
    }

    /**
     * Método que permite ingresar a comprar a Ripley como usuario Ya registrado previamente en el Portal
     * @param rutUsuarioRegistrado Rut del usuario registrado en formato String
     * @param claveUsuarioRegistrado Clave del usaurio registrado en formato String
     */
    public void ingresoComoUsuarioRegistrado(String rutUsuarioRegistrado, String claveUsuarioRegistrado){
        // Espera de contenedor y elementos
        waitForElementToBeVisible(this.contenedorLoginCheckout);
        waitForElementToBeVisible(this.campoRutUsuarioRegistrado);

        // Ingreso de datos
        this.campoRutUsuarioRegistrado.sendKeys(rutUsuarioRegistrado);
        this.campoClaveUsuarioRegistrado.sendKeys(claveUsuarioRegistrado);

        // Espero carga de botón ingresar y hago click en el mismo botón
        waitForElementToBeClickable(this.btnIngresarComoUsuarioRegistrado);
        this.btnIngresarComoUsuarioRegistrado.click();
    }
}
