package corp.ripley.mtochile.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/features"},
        tags = {"@regresionDD"},
        glue = {"corp/ripley/mtochile/stepdefinition"},
        plugin = {"pretty",
                "html:target/cucumber-reports",
                "junit:target/cucumber-reports/report.xml",
                "json:target/cucumber-reports/report.json",}
)
public class RunCucumberTest {}