@regresionDD
Feature: Visualizacion de compras en MTO
  El objetivo de este feature es probar distintas casuisticas relacionadas a compras en TV
  con distintas formas de compra.

  Scenario Outline: Compra exitosa como usuario invitado, despacho a domicilio y tarjeta bancaria
    Given que genero compra con datos: "<sku>" "<pan>" "<CVV>" "<rut>" "<clave>"
    And inserto OC a MTO
    When abro MTO en Chile
    Then la visualizo en MTO Chile
    And el estado es "<estado>"

    Examples:
      | sku           |  pan               | CVV | rut       | clave | estado |
      | 2094020940000 |  4051885600446623  | 123 | 111111111 | 123   | Fin    |

  Scenario Outline: Compra exitosa como usuario logueado, despacho a domicilio y tarjeta Ripley
    Given que genero compra tarjeta Ripley con datos: "<sku>" "<pan>" "<CVV>" "<rut>" "<clave>"
    And inserto OC a MTO
    When abro MTO en Chile
    Then la visualizo en MTO Chile
    And el estado es "<estado>"

    Examples:
      | sku           |  pan               | CVV | rut       | clave | estado |
      | 2094020940000 |  5490702860677647  | 591 | 58540501  | 1234  | Fin    |